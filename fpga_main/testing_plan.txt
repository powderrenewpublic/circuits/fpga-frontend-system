Connectors
    GPIO
        RF
            a0
            a1
            b0
            b1
            c0
            c1
            d0
            d1
            MISC IO
    
        DEBUG
        CLOCK PPS
        Memory Socket
    
    Clock Outputs
        Clock A
        Clock B
    
    Power
        RF
            power a
            power b
        
        Sockets
            a0
            a1
            a2
            ba
            bb
            bc
    
    Bottom IO
        0
        1
    
    ADC
        Non RF
        RF
            0
            1
    
    SD Card

Peripherals
    External Configuration Flash
    MAC EEPROM
    DCDC Regulators
        Synchronization
    PLL
        Measure free running clock frequency
    I2C Muxes
    I2C GPIO Expanders



Tests:

Connectors.GPIO.*:
* Test sequentially one at a time
* Test each line as an input, pull line low and high, make sure only that line
  sees a change.
* Connect the ground pins separately, load 10 mA each and assure no more than
  0.1 V rise (should be trivial).
* For all I/O that provides an I/O voltage supply put a 10 mA load on each pin
  (split the pins on the test side) and assure no more than a 0.1 V drop from
  the ideal supply.


Connectors.Clock Outputs.*:
* Put a 100 ohm differential load on and make sure voltage swing is at least
* Check that this clock is operating and locked to the clock the PLL sees.
* Provide a clock input to the FPGA, have it lock the PLL to this and check
  that the outputs are locked to this clock.
* Create IP core to count (to 100e6) master clock and compare it to PLL clock

Connectors.CLOCK PPS:
* Provide 10 MHz clock on clock line test locking 40 MHz using 10 MHz lock
* Check edges of 10MHz and pps
    - 10 ns setup/hold
* Check GPIO

Connectors.Power.RF.*:
* For power rails supplying the power boards
    - Check each pin at maximum of 500 mA or the specified current.
    - Check that voltage does not drop by 0.1 V.
    - Check the ground bus. 500 mA per pin, does not rise by 0.1 V.
* Check all GPIO lines

Connectors.Power.RF.B*:
* For power rails supplying the frontend:
    - Connect each pin individually at both ends, present 1 ohm of resistance
      per pin on power board side, 0.66 ohm of resistance per pin on FFC side.
    - Load to 500 mA per pin.
    - Similarly test ground rails are well connected.
    - Test under load for power handling performance (total of 2A for each
      channel).
    - Check voltage does not fall or rise by 0.1 V on power or ground.

Connectors.Power.RF.A*:
* Same as with Power.RF.B* except simple 1 ohm of resistance per pin on both
  sides.

Connectors.Bottom IO.*:
* Treat GPIO as with Connectors.GPIO.*.
* Treat power to similar 500 mA per pin testing.
    - Careful, there are 2A fuses present

Connectors.ADC.*:
* Check each channel can read minimum/maximum and at least one mid-scale
  value.
* Check each pin one at a time and that other pin values are not affected by
  channel being tested.

Connectors.SD Card:
* Check that SD card works.
* Check that SD card work in quad bus mode (using all 4 data lines, not just
  SPI).

Peripherals.External Configuration Flash:
* Check that flash can be programmed (using JTAG should be possible)
* Check that FPGA will boot from flash (use a different program from what is
  on the FPGA, use the CFG pin to force boot from flash instead of FPGA
  internal).

Peripherals.MAC EEPROM:
* Get MAC address out.
    - Report the MAC address
* Write something to the non-MAC area of the EEPROM and read it back after
  power cycle.

Peripherals.DCDC Regulators:
* Test each regulator to specified limits.
    - Where reasonable on first rev of board
* Test that DCDC Regulator can be synchronized to a signal from the FPGA
  (likely this involves using oscilloscope)
    - Or use op amp or similar circuit to feed signal back into FPGA
* Test DCDC Regulator can be synchronized FPGA, to crystal, FPGA can be reset and
  can re-sync to FPGA without DCDC regulator getting a hiccup.

Peripherals.PLL:
* As with connector above, check PLL will sync to provided signal.
* Measure free-running crystal frequency.
    - TCM2-1T+

I2C Muxes:
* Check that I2C Muxes can switch to all necessary outputs.
    - Will be necessary to test all onboard I2C devices
    - Also check buses on power board connectors (custom power board)

I2C GPIO Expanders:
* Check all I/Os on all GPIO expanders.
* Use procedure similar to Connecturs.GPIO.*.
* Will require custom power board to break out these pins.

FPGA:
* Use shift register and lfsr generator to load fpga for burn in testing


Sam
    NEORV32 Core
    Serial Peripherals
    PLL

Gary
    Adapter Boards
        Power A
        Power B
        Memory
        Clock PPS
        Clock A/B
        RF Misc IO
        Non RF Analog

    SD Card
    For boards with power, use 1k ohm load on each power pin and verify proper voltage

Alex
    Weird Vias @(118, 122) (1.2V net)
