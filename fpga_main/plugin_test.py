from shapely.prepared import prep

from pcbnew import wxPoint

import kikit.plugin
from kikit import substrate
from kikit.common import (shpBBoxLeft, shpBBoxRight, shpBBoxTop, shpBBoxBottom,
                          SHP_EPSILON)
from kikit import units
from kikit.panelize_ui_impl import dummyFramingSubstrate
from kikit.panelize import prolongCut as prolong_cut



class TabPlugin(kikit.plugin.TabsPlugin):
    def __init__(self, preset, userArg):
        super().__init__(preset, userArg)
        print("TabPlugin inited")

    def _buildTabAnnotations(self, panel, countFn, widthFn, ghostSubstrates):
        """
        Add tab annotations for the individual substrates based on their
        bounding boxes. Assign tabs annotations to the edges of the bounding
        box. You provide a function countFn, widthFn that take edge length and
        direction that return number of tabs per edge or tab width
        respectively.
        You can also specify ghost substrates (for the future framing).
        """
        neighbors = substrate.SubstrateNeighbors(panel.substrates + ghostSubstrates)
        S = substrate.SubstrateNeighbors
        sides = [
            (S.leftC, shpBBoxLeft, [1, 0]),
            (S.rightC, shpBBoxRight,[-1, 0]),
            (S.topC, shpBBoxTop, [0, 1]),
            (S.bottomC, shpBBoxBottom,[0, -1])
        ]
        for i, s in enumerate(panel.substrates):
            for query, side, dir_ in sides:
                for n, shadow in query(neighbors, s):
                    edge = side(s.bounds())
                    for section in shadow.intervals:
                        edge.min, edge.max = section.min, section.max
                        tWidth = widthFn(edge.length, dir_)
                        tCount = countFn(edge.length, dir_)
                        a = panel._buildTabAnnotationForEdge(edge, dir_, tCount,
                                                            tWidth)
                        panel.substrates[i].annotations.extend(a)

    def buildTabAnnotations(self, panel):
        def width_fn(edge_length, dir_):
            if dir_[1] == -1:
                return 0
            else:
                return 2.8 * units.mm

        def count_fn(edge_length, dir_):
            if dir_[1] == -1:
                return 0
            else:
                return 3

        framing_substrate = dummyFramingSubstrate(panel.substrates,
                                                  self.preset)
        self._buildTabAnnotations(panel, count_fn, width_fn, framing_substrate)


class MouseBitesPlugin(kikit.plugin.CutsPlugin):
    def __init__(self, preset, userArg):
        super().__init__(preset, userArg)
        print("MouseBitesPlugin inited")
        options = preset["cuts"]
        self.drill = options["drill"]
        self.spacing = options["spacing"]
        self.offset = options["offset"]

        self.prolongation_tab = options["prolong"]
        self.prolongation_other = options["prolong"]

        if "prolong_tab" in options:
            self.prolongation_tab = units.readLength(options["prolong_tab"])
        if "prolong_other" in options:
            self.prolongation_other = \
                    units.readLength(options["prolong_other"])

    def _render_tab_cuts(self, panel, cuts, prolongation):
        bloatedSubstrate = prep(
            panel.boardSubstrate.substrates.buffer(SHP_EPSILON))

        for cut in cuts:
            cut = cut.simplify(SHP_EPSILON) # Remove self-intersecting geometry
            cut = prolong_cut(cut, prolongation)
            offset_cut = cut.parallel_offset(self.offset, "left")
            length = offset_cut.length

            count = int(length / self.spacing)

            for i in range(count):
                # In the case of 1 hole, put it at 1/2
                # In the case of 2 holes, put them at 1/4 and 3/4
                # In the case of 3 holes, put them at 1/6, 1/2 and 5/6
                hole = offset_cut.interpolate( (i+0.5) * length / (count) )

                if bloatedSubstrate.intersects(hole):
                    panel.addNPTHole(wxPoint(hole.x, hole.y), self.drill)

    def renderTabCuts(self, panel, cuts):
        self._render_tab_cuts(panel, cuts, self.prolongation_tab)

    def renderOtherCuts(self, panel, cuts):
        self._render_tab_cuts(panel, cuts, self.prolongation_other)
