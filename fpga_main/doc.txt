Interfaces:
* RF ADC
    - 8x channels 8-bit ADC 1 MSPS (switched between channels)
    - 8x channels 12-bit ADC 1 MSPS (switched between channels)
    - 2x 20-pin FPC connectors
* Non-RF ADC
    - 20 analog channels
    - Analog switched to 8 channel ADC
    - 8-bit
    - 1 MSPS between all 20 channels
    - Shared with Power A sensor channels and offboard channel
    - If Power A modules not in use can be used off board instead
    - 1x 40-pin FPC connector under Power A area
* Bottom Interface
    - Bring in 12V (required)
    - Optionally bring in 48V
    - 40 GPIOs
    - Arranged as 20 matched pair lines 100 ohm differential impedance
    - Terminate on connected board (pairs can be used either direction)
* Memory Interface
    - 44 GPIOs
    - Mini-PCIe connector (52-pin)
    - 3.3V provided and 3.3V I/O
    - Enough for e.g. 16 Mbit (16x1M) SRAM
* MicroSD slot
    - 6 GPIO
    - 3.3V
* 10 MHz/PPS Interface
    - 3.3V
    - 8 misc GPIO
    - 1 clock pin for 10 MHz
    - 1 GPIO for PPS
* Misc GPIO Human Interface
    - 8 GPIO
    - For e.g.
        o LEDs
        o Push buttons
    - 10 pin FPC connector
    - 3.3V I/O
    - 3.3V/GND on connector
* Misc Debug Connector
    - 16 GPIO
    - 3.3V I/O
    - 3.3V/GND provided
    - 20 pin FPC connector
* JTAG Connector
    - Weather Proof Connector
    - JTAG signals + V_JTAG
    - 3.3V able to be used on loopback for V_JTAG for programmers w/o V_JTAG
      out
* Power modules
    - 3x Power A sockets
        o 6x Output Voltage Channels, up to 500 mA each (connector)
        o 48V, 12V, 6V, 4.3V, GND
        o 2 Sync signals
        o V_IO supply
        o 1x I2C interface
        o 6x Power Good signals
        o 6x Enable signals
        o 6x V Sense lines
    - 3x Power B sockets
        o 1x Output Voltage Channel, total of 2 A (connector)
        o 48V, 12V, 5V, 3.3V, GND
        o 2x I2C channels
        o 11x medium activity GPIO (EN, PG, etc)
        o 11x slower activity GPIO (mode setting)
        o Sync signal and sync enable signal
* RF Interface (other than ADC)
    - Myriad digital connections, mostly 2.5V I/O, some 3.3V I/O
    - Power from power modules, cleaned a tad and passed on
    - Multiple FPC connectors


On board resources:
* FPGA
* Power
* MAC EEPROM
* ADCs
* VCXO and PLL (for locking to another signal, e.g. 10 MHz)
* SPI EEPROM for FPGA code
* 1.2 MHz oscillator and switching to keep regulators stable during
  reprogramming REFRESH cycle.
