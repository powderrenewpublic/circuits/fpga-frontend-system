* Power filter
* Node 1 is input voltage
* Node 2 is output node
L1 1 5 0.1u
R4 5 3 0.02
R3 1 3 1
C1 3 4 10u
C2 3 5 300u
R1 4 0 0.01
R2 5 0 0.051
RCON 3 2 0.001
ROUT 2 0 0.5
CIN 1 7 200u
RCIN 7 0 0.1
CIN2 1 8 20u
RCIN2 8 0 0.01
RIN 1 6 100m
LIN 6 0 7.5p
VIN 6 0 dc 0 ac 1
*IOUT 2 0 dc 0 ac 1
.ac dec 100 1 100Meg
.control
run
plot vdb(1) xlog
plot vdb(2) xlog
.endc
.end
