# FPGA Main

## Connectors

### Debug Connector

#### Pinout

| Pin          | FPGA Function | Note |
| ------------ | ------------- | ---- |
| DEBUG_GPIO0  |  INITN        | fixed function when using dual boot feature |
| DEBUG_GPIO1  |  CSSPISN      | can be used as an input |
| DEBUG_GPIO2  |  DONE         | fixed function when using dual boot feature |
| DEBUG_GPIO3  |  PROGRAMN     | fixed function when using dual boot feature |
| DEBUG_GPIO4  |  IO           | |
| DEBUG_GPIO5  |  IO           | |
| DEBUG_GPIO6  |  IO           | |
| DEBUG_GPIO7  |  IO           | |
| DEBUG_GPIO8  |  IO           | |
| DEBUG_GPIO9  |  CFG1         | fixed function when using dual boot feature |
| DEBUG_GPIO10 |  SI           | connected to spi eeprom |
| DEBUG_GPIO11 |  SO           | connected to spi eeprom |
| DEBUG_GPIO12 |  CCLK         | connected to spi eeprom |
| DEBUG_GPIO13 |  CSSPIN       | connected to spi eeprom |
| DEBUG_GPIO14 |  NC           | spi eeprom write protect |
| DEBUG_GPIO15 |  NC           | sets U401, U402 I2C Address A2 |

### RF ADC Connectors

There are 16 RF ADC channels, numbered 0 to 15. Each signal is filtered with a 10 kHz lowpass filter. The `RF ADC 0` connector has channels 0 to 7 which are connected to an [`ADS7028`] ADC. The `RF ADC 1` connector has channels 8 to 15 which are connected to an [`ADC088S102`] ADC.

| Connector | Channels | ADC |
| --------- | -------- | --- |
| RF ADC 0  | 0 to 7   | [`ADS7028`] |
| RF ADC 1  | 8 to 15  | [`ADC088S102`] |

| ADC        | Sample Rate | Resolution (bits) | Note |
| ---------- | -----------:| -----------------:| ---- |
| ADS7028    | 1 MSPS      | 8                 | No programming registers |
| ADC088S102 | 1 MSPS      | 12                | 16-bit true RMS option w/ window |

### Non RF ADC Connectors

There are 20 Non RF ADC channels, numbered 0 to 19. Each signal is filtered with a 10 kHz lowpass filter. All 20 channels connected to the Non RF ADC connector. Additionally, channels 0 through 17 are connected to the `V_SENSE[0-17]` `Power Socket A` signals. All 20 channels are muxed to a single [`ADC088S102`]. Note that there is a transient when changing the mux select due to the output enable of the analog mux being fixed on.


[`ADS7028`]: https://www.ti.com/lit/ds/symlink/ads7028.pdf
[`ADC088S102`]: https://www.ti.com/lit/ds/symlink/adc088s102.pdf
